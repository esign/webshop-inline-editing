/**
 * Created by Bart Decorte on 5/02/16.
 */

function Unleash() {

	this.currentlyEditing = null;
	this.loads = 0;
	this.editing = true;

	this.gaData = null;
	this.disallowedClasses = ['fadeIn'];

	this.init = function() {
		var that = this;
		that.wrapContents();
		//that.addAnalyticsToDOM();
		that.wrapElements();
		that.bindListeners();
	};

	this.accentColor = tinycolor('#ec0b43');

	this.isIE = function(){
		var ua = window.navigator.userAgent;
		var msie = ua.indexOf("MSIE ");

		return (msie > 0);
	};

	this.bindListeners = function() {
		var that = this;

		$('body').on('click', '.inline-edit-link', function(ev){ return that.listeners.edit(ev, that) });

		if (that.isIE()) {
			$('body').on("propertychange", function () { that.wrapElements() });
		} else {
			$('body').on("DOMSubtreeModified", function () { that.wrapElements() });
		}

		$('.inline-edit-seo-toggle').click(function(ev){ that.listeners.seoClick(ev, that) });

		$('.inline-edit-toggle').click(function(ev){ that.listeners.editToggle(ev, that) });

		$(document).on('DOMMouseScroll mousewheel', '.inline-edit-prevent-parent-scroll', function(ev) {
			var $this = $(this),
				scrollTop = this.scrollTop,
				scrollHeight = this.scrollHeight,
				height = $this.height(),
				delta = (ev.type == 'DOMMouseScroll' ?
				ev.originalEvent.detail * -40 :
					ev.originalEvent.wheelDelta),
				up = delta > 0;

			var prevent = function() {
				ev.stopPropagation();
				ev.preventDefault();
				ev.returnValue = false;
				return false;
			};

			if (!up && -delta > scrollHeight - height - scrollTop) {
				$this.scrollTop(scrollHeight);
				return prevent();
			} else if (up && delta > scrollTop) {
				$this.scrollTop(0);
				return prevent();
			}
		});
	};

	this.listeners = {
		edit: function(ev, scope) {
			ev.preventDefault();

			var elt = $(ev.target).get(0).tagName === 'SPAN' ? $(ev.target) : $(ev.target).parents('span').first();
			var anchor = $(elt).get(0).tagName === 'SPAN' ? elt : $(elt).parents('span').first();
			var src = $(anchor).attr('data-href') + '?ie=true';

			$.fancybox.open({
				'href': src,
				'type':	'iframe',
				'width': '780',
				'height': '780',
				'helpers': {
					'overlay': {
						'locked': false
					}
				},
				'beforeShow' : function(){
					console.log(this);
					$(".fancybox-wrap").addClass("inline-edit-fancybox");
					if (!$('.fancybox-overlay').hasClass('inline-edit-fancybox-overlay')) {
						$('.fancybox-overlay').addClass('inline-edit-fancybox-overlay');
					}
				},
				'beforeLoad': function(){
					if ($('.fancybox-overlay').hasClass('inline-edit-fancybox-overlay')) {
						$('.fancybox-overlay').removeClass('inline-edit-fancybox-overlay');
					}
				}
			});

			// when clicked in iframe (as is the case with emails), wrong window instance has focus and ESC won't work
			$(window).focus();

			if ($('iframe[src="' + src + '"]').length == 0) {
				setTimeout(function(){
					scope.listeners.edit(elt, scope);
				}, 100)
			} else {
				$('iframe[src="' + src + '"]').load(function () {
					scope.loads++;
					if (scope.loads > 1) {
						scope.refresh(scope.currentlyEditing);
					}
				});

				scope.currentlyEditing = $(anchor).attr('id');
				scope.loads = 0;
			}

			return false;
		},
		translationHoverIn: function(ev, scope) {
			var $elt = $(ev.target).get(0).tagName === 'SPAN' ? $(ev.target) : $(ev.target).parents('span').first();
			scope.findGlobal($('body'), $elt.attr('data-inline-edit-content')).stop().animate({"opacity": "0.6"}, '220');
		},
		translationHoverOut: function(ev, scope) {
			var $elt = $(ev.target).get(0).tagName === 'SPAN' ? $(ev.target) : $(ev.target).parents('span').first();
			scope.findGlobal($('body'), $elt.attr('data-inline-edit-content')).stop().animate({"opacity": "1"}, '220');
		},
		seoClick: function(ev, scope) {
			$('.inline-edit-seo-toggle').toggleClass('active');
			$('.inline-edit-seo').toggleClass('open');
		},
		editToggle: function(ev, scope) {
			scope.findGlobal($('body'), '.inline-edit-toggle').toggleClass('active');
			scope.toggleEditing();
		}
	};

	this.refresh = function(id){
		var that = this;

		var refreshUrl = that.findGlobal($('body'), '#' + id).first().data('inline-edit-refresh-url');
		//console.log(refreshUrl);

		$.ajax({
			method: 'get',
			url: refreshUrl !== 'undefined' ? refreshUrl : window.location.href,
			dataType: 'html',
			success: function(data){
				//console.log(data);
				that.processRefresh(data, id);
			},
			error: function(){ /*console.log('error')*/ },
			complete: function(){ /*console.log('complete')*/ }
		});
	};

	// Performs a search, frames included
	this.findGlobal = function(context, selector){
		var matches = $(context).find(selector);
		var iframes = $(context).find('iframe');

		var i, iframe;
		for (i=0; i<iframes.length; i++) {
			try {
				iframe = iframes[i];
				matches = $(matches).add($(iframe).contents().find(selector));
			} catch (e) {
				// error will be thrown when trying to access iframe of different origin
			}
		}
		return matches;
	};

	this.processRefresh = function(data, id) {
		var that = this;
		var $source = jQuery(data);

		var $link = that.findGlobal($('body'), '#' + id).first();

		var $oldElt, $newElt;
		if (typeof $link.attr('date-inline-edit-parent') !== typeof undefined && $link.attr('date-inline-edit-parent') !== false) {
			$oldElt = that.findGlobal($('body'), '#' + id).first().parents($link.attr('date-inline-edit-parent')).first();
			$newElt = that.findGlobal($source, '#' + id).first().parents($link.attr('date-inline-edit-parent')).first();
		} else {
			$oldElt = that.findGlobal($('body'), '#' + id).first().parent();
			$newElt = that.findGlobal($source, '#' + id).first().parent();
		}

		var disallowedSelector = '';
		var i;
		for (i=0; i<that.disallowedClasses.length; i++) {
			if (i == 0) {
				disallowedSelector += '.' + that.disallowedClasses[i];
			} else {
				disallowedSelector += ', .' + that.disallowedClasses[i];
			}
		}
		var childrenWithDisallowedClasses = $newElt.find(disallowedSelector);
		var j;
		for (i=0; i<childrenWithDisallowedClasses.length; i++) {
			for (j=0; j<that.disallowedClasses.length; j++) {
				if ($(childrenWithDisallowedClasses[i]).hasClass(that.disallowedClasses[j])) {
					$(childrenWithDisallowedClasses[i]).removeClass(that.disallowedClasses[j]);
				}
			}
		}

		$newElt.css({
			display: $oldElt.css('display'),
			visibility: $oldElt.css('visibility'),
			opacity: $oldElt.css('opacity'),
			position: $oldElt.css('position')
		});
		$newElt = that.wrapElement($newElt);

		var $isoParent = that.findGlobal($('body'), '#' + id).first().parents('[data-isotope]').first();
		if ($isoParent.length > 0) {
			// HANDLE ISOTOPE
			$oldElt.replaceWith($newElt);
			$($isoParent).isotope('reloadItems').isotope({ sortBy: 'original-order' });

		} else {
			// SIMPLE REPLACE
			$oldElt.replaceWith($newElt);
		}

		$(window).resize();
	};

	this.wrapElements = function() {
		var that = this;
		$('.inline-edit-link').each(function(){
			that.wrapElement(this);
		});
	};

	this.wrapElement = function(elt){
		var scope = this;
		var $link = $(elt);

		if (!$link.hasClass('inline-edit-link')) {
			$link = $link.find('.inline-edit-link').first();
		}

		$link.off('mouseenter').off('mouseleave').hover(
			function(ev){ scope.listeners.translationHoverIn(ev, scope) },
			function(ev){ scope.listeners.translationHoverOut(ev, scope) }
		);

		if ($link.parent().parent().hasClass('inline-edit-subject')) {
			if ($link.parent().parent().css('position') == 'static') {
				$link.css('margin-top', '2px').parent().parent().css('position', 'relative');
			}
		} else {
			if ($link.parent().css('position') == 'static') {
				$link.parent().css('position', 'relative');
			}
		}

		return elt;
	};

	this.wrapContents = function() {
		$('body').children().wrapAll('<div class="inline-edit-body-wrap"/>');
	};

	this.confirm = function(html, confirmed, canceled) {
		var html =
				'<div class="inline-edit-popup inline-edit-hide inline-edit-prevent-parent-scroll inline-edit-va-wrap inline-edit-confirm">' +
				'<div class="inline-edit-va-m inline-edit-a-c">' +
				'<div class="inline-edit-popup-inner inline-edit-opt-l inline-edit-a-l">' +
				'<div class="inline-edit-a-c"><i class="inline-edit-icon-esign"></i></div>' +
				'<div class="inline-edit-sure inline-edit-a-c">Are you sure you want to do that?</div>' +
				html +
				'<div class="inline-edit-popup-buttons">' +
				'<div class="inline-edit-popup-button inline-edit-popup-button-positive">Yes</div>'+
				'<div class="inline-edit-popup-button inline-edit-popup-button-negative">No</div>'+
				'</div>'+
				'</div>' +
				'</div>' +
				'</div>'
			;

		$('body').append(html);
		$('.inline-edit-confirm').removeClass('inline-edit-hide');

		var close = function(){
			$('.inline-edit-confirm').addClass('inline-edit-hide');
			setTimeout(function(){
				$('.inline-edit-confirm').detach();
			}, 250)
		};
		$('.inline-edit-confirm .inline-edit-popup-button-positive').click(function(){
			confirmed();
			close();
		});
		$('.inline-edit-confirm .inline-edit-popup-button-negative').click(function(){
			canceled();
			close();
		});
	};

	this.success = function(html, closed) {
		var html =
				'<div class="inline-edit-popup inline-edit-hide inline-edit-prevent-parent-scroll inline-edit-va-wrap inline-edit-success">' +
				'<div class="inline-edit-va-m inline-edit-a-c">' +
				'<div class="inline-edit-popup-inner inline-edit-opt-l inline-edit-a-l">' +
				'<div class="inline-edit-a-c"><i class="inline-edit-icon-esign"></i></div>' +
				'<div class="inline-edit-sure inline-edit-a-c">Success!</div>' +
				html +
				'<div class="inline-edit-popup-buttons">' +
				'<div class="inline-edit-popup-button inline-edit-popup-button-positive">OK</div>'+
				'</div>'+
				'</div>' +
				'</div>' +
				'</div>'
			;

		$('body').append(html);
		$('.inline-edit-success').removeClass('inline-edit-hide');

		var close = function(){
			$('.inline-edit-success').addClass('inline-edit-hide');
			setTimeout(function(){
				$('.inline-edit-success').detach();
			}, 250)
		};
		$('.inline-edit-success .inline-edit-popup-button-positive').click(function(){
			close();
			closed();
		});
	};

	this.error = function(html, closed) {
		var html =
				'<div class="inline-edit-popup inline-edit-hide inline-edit-prevent-parent-scroll inline-edit-va-wrap inline-edit-error">' +
				'<div class="inline-edit-va-m inline-edit-a-c">' +
				'<div class="inline-edit-popup-inner inline-edit-opt-l inline-edit-a-l">' +
				'<div class="inline-edit-a-c"><i class="inline-edit-icon-esign"></i></div>' +
				'<div class="inline-edit-sure inline-edit-a-c">Error :(</div>' +
				html +
				'<div class="inline-edit-popup-buttons">' +
				'<div class="inline-edit-popup-button inline-edit-popup-button-positive">OK</div>'+
				'</div>'+
				'</div>' +
				'</div>' +
				'</div>'
			;

		$('body').append(html);
		$('.inline-edit-error').removeClass('inline-edit-hide');

		var close = function(){
			$('.inline-edit-error').addClass('inline-edit-hide');
			setTimeout(function(){
				$('.inline-edit-error').detach();
			}, 250)
		};
		$('.inline-edit-error .inline-edit-popup-button-positive').click(function(){
			closed();
			close();
		});
	};

	// Not ready for production
	this.toggleEditing = function() {
		var that = this;
		that.editing = !that.editing;
		that.findGlobal($('body'), '[id^="inline-edit-link"]').not($('.inline-edit-seo').find('[id^="inline-edit-link"]')).toggle();
	};

	// jQuery collection to html string
	this.toHTML = function(elts) {
		var html = '';

		var i, clone;
		for (i=0; i<elts.length; i++) {
			clone = $(elts[i]).clone();
			html += $('<div>').append(clone).html();
		}

		return html;
	};

	this.decodeEntities = function(encodedString) {
		var textArea = document.createElement('textarea');
		textArea.innerHTML = encodedString;
		return textArea.value;
	};

	this.emailValid = function(email) {
		var scope = this;

		var regex = new RegExp(/[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/);
		return email.match(regex) ? true : false;
	};

	this.init();

}

$(document).ready(function(){
	new Unleash();
});