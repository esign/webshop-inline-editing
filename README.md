## Installation

### Getting the package
#### Composer

Add *Esign Webshop Inline Editing* to your `composer.json` file.

    "esign/webshop-inline-editing": "~1.2.0"
    
Add this after the "require-dev" part, to tell Composer to look for the repo:

    "repositories": [
            {
                "type": "git",
                "url": "git@gitlab.com:esign/webshop-inline-editing.git"
            }
        ],

Run `composer update` to get the latest version of the package.

Permission error? Make sure you've been added to the Gitlab project and your ssh key is active there.
For read-only access (production server), you can use deploy keys from Gitlab.

#### Manually

It's recommended that you use Composer, however you can download and install from this repository.

## Laravel

*Esign Webshop Inline Editing* comes with a service provider for Laravel. You'll need to add it to your `composer.json` as mentioned in the above steps, then register the service provider with your application.

Open `config/app.php` and find the `providers` key. Add `WebshopInlineEditingServiceProvider ` to the array.

```php
...
Esign\WebshopInlineEditing\WebshopInlineEditingServiceProvider::class,
...
```

You can also add an alias to the list of class aliases in the same file.

```php
...
'InlineEditing' => Esign\WebshopInlineEditing\Facades\InlineEditing::class
...
```
## Config

In order to edit the default configuration for this package you may execute:

```
php artisan vendor:publish --provider="Esign\WebshopInlineEditing\WebshopInlineEditingServiceProvider"
```

After that, `config/webshop-inline-editing.php` will be created. Inside this file you will find all the fields that can be edited in this package.

## Usage

Add the following code in the `report` method in `app/Exceptions/Handler.php`

```php
...

{{ ie('term') }}

...

```

Alternatively, a Blade directive can be created in `app/Providers/AppServiceProvider`, like so:
```php
public function boot()
{
    ...
    
    Blade::directive('ie', function ($term) {
        return "<?php echo ie($term) ?>";
    });
    
    ...
}
```

After which the same result can be achieved using:
```php
...

@ie('term')

...

```