<?php
/**
 * Created by PhpStorm.
 * User: bartdecorte
 * Date: 03/12/2016
 * Time: 10:31
 */

namespace Esign\WebshopInlineEditing;

class Util {

    static function cssMultiSelector ($single_selectors) {
        $selector = '';

        $counter = 0;
        foreach ($single_selectors as $val) {
            $selector .= ($counter > 0 ? ', ' : '');
            $selector .= $val;
            $counter++;
        }

        return $selector;
    }

    static function adjustBrightness ($hex, $steps) {
        // Steps should be between -255 and 255. Negative = darker, positive = lighter
        $steps = max(-255, min(255, $steps));

        // Normalize into a six character long hex string
        $hex = str_replace('#', '', $hex);
        if (strlen($hex) == 3) {
            $hex = str_repeat(substr($hex,0,1), 2).str_repeat(substr($hex,1,1), 2).str_repeat(substr($hex,2,1), 2);
        }

        // Split into three parts: R, G and B
        $colorParts = str_split($hex, 2);
        $return = '#';

        foreach ($colorParts as $color) {
            $color   = hexdec($color); // Convert to decimal
            $color   = max(0,min(255,$color + $steps)); // Adjust color
            $return .= str_pad(dechex($color), 2, '0', STR_PAD_LEFT); // Make two char hex code
        }

        return $return;
    }

}