<?php
/**
 * Created by PhpStorm.
 * User: bartdecorte
 * Date: 03/12/2016
 * Time: 11:30
 */

Route::group(['middleware' => ['web']], function () {
    Route::get('inline-editing/enable', ['as' => 'webshop-inline-editing.enable', 'uses' => '\Esign\WebshopInlineEditing\WebshopInlineEditingController@enable']);
    Route::get('inline-editing/disable', ['as' => 'webshop-inline-editing.disable', 'uses' => '\Esign\WebshopInlineEditing\WebshopInlineEditingController@disable']);
});