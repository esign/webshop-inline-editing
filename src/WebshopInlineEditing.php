<?php
/**
 * Created by PhpStorm.
 * User: bartdecorte
 * Date: 03/12/2016
 * Time: 09:41
 */

namespace Esign\WebshopInlineEditing;

use Illuminate\Support\Facades\DB;
use Mcamara\LaravelLocalization\Facades\LaravelLocalization;

class WebshopInlineEditing {

    static $dummy = array(
        'first_name' => 'Bart',
        'last_name' => 'Decorte',
        'name' => 'Bart Decorte',
        'street' => 'Stropkaai',
        'street_number' => '55',
        'zip' => '9000',
        'city' => 'Ghent',
        'mailbox' => '1',
        'country' => 'België',
        'phone' => '+32 9 241 86 50',
        'email' => 'hello@esign.eu',
        'plain_body' => 'McCluskey asked with real interest, “Is the Italian food good here?” Sollozzo reassured him. “Try the veal, it’s the finest in New York.” The solitary waiter had brought a bottle of wine to the table and uncorked it. He poured three glasses full. Surprisingly McCluskey did not drink. “I must be the only Irishman who don’t take the booze,” he said. “I seen too many good people get in trouble because of the booze.” Sollozzo said placatingly to the captain, “I am going to talk Italian to Mike, not because I don’t trust you but because I can’t explain myself properly in English and I want to convince Mike that I mean well, that it’s to everybody’s advantage for us to come to an agreement tonight. Don’t be insulted by this, it’s not that I don’t trust you.”',
        'title' => 'Sollozzo refusing a booth',
        'excerpt' => 'The three of them sat at the only round table, Sollozzo refusing a booth. There were only two other people in the restaurant. Michael wondered whether they were Sollozzo plants. But it didn’t matter. Before they could interfere it would be all over.',
    );

    static $features = array(
        'inline-editing',
        'seo',
        'analytics',
        'mailing'
    );

    public $editMode = false;

    private $configName = 'webshop-inline-editing';
    private $locale = null;
    private $supportedLocales = [];
    private $table = null;
    private $tlFunctionExists = false;

    public function __construct()
    {
        $this->locale = LaravelLocalization::getCurrentLocale();
        $this->supportedLocales = LaravelLocalization::getSupportedLanguagesKeys();
        $this->table = config($this->configName . '.database.table');
        $this->tlFunctionExists = function_exists('tl');

        if (session('webshop-inline-editing') === true) $this->editMode = true;
    }

    private function assetPath($path) {
        return asset('esign/webshop-inline-editing/' . $path);
    }

    private function tl($term, $variables = []) {
        if ($this->tlFunctionExists) return tl($term, $variables);

        $translation = DB::table($this->table)
            ->select('id', 'column', 'value', 'type')
            ->whereNull('table')
            ->whereNull('row')
            ->where('column', $term)
            ->where('locale', $this->locale)
            ->first();

        if (is_null($translation)) {
            $translation = DB::table($this->table)
                ->insert([
                    'column' => $term,
                    'type' => 'text',
                    'locale' => $this->locale
                ]);
			$translation = DB::table($this->table)
				->select('id', 'column', 'value', 'type')
				->whereNull('table')
				->whereNull('row')
				->where('column', $term)
				->where('locale', $this->locale)
				->first();
        }

        if (empty($translation->value)) return $term;
        return $this->injectVariables($translation->value, $variables);
    }

    private function injectVariables($translation, $variables = []) {
        foreach ($variables as $name => $value) {
            $translation = preg_replace('/\{\{\s*' . $name . '\s*\}\}/', $value, $translation);
        }
        return $translation;
    }

    /**
     * Generates translation edit link
     * @param $term: translation term
     * @param $parent: jQuery selector to identify parent element to be refreshed after editing
     * @param $refresh_url: in case of asynchronously loaded content, URL where the new HTML can be fetched
     * @return string
     */
    public function render($term, $parent = null, $refresh_url = null, $variables = [])
    {

        if($this->editMode)
        {
            $translation = DB::table($this->table)
                ->select('id', 'column', 'value', 'type')
                ->whereNull('table')
                ->whereNull('row')
                ->where('column', $term)
				->where('locale', $this->locale)
                ->first();

            if (is_null($translation)) {
                $translation = DB::table($this->table)
                    ->insert([
                        'column' => $term,
                        'type' => 'text',
                        'locale' => $this->locale
                    ]);
				$translation = DB::table($this->table)
					->select('id', 'column', 'value', 'type')
					->whereNull('table')
					->whereNull('row')
					->where('column', $term)
					->where('locale', $this->locale)
					->first();
            }

            $text = !empty($translation->value) ? $this->injectVariables($translation->value, $variables) : $translation->column;

            $content = '<span id="inline-edit-' . $translation->id . '">' . $text . '</span>';
            $link = $this->editLink($translation->id, $translation->column, $parent, $refresh_url, !config($this->configName . '.button_labels'));
            return $link . $content;

        }

        return $this->tl($term, $variables);
    }

    /**
     * Generates entity edit link
     * @param $table: Configured alias for the module (libraries/unleash/config.php)
     * @param $id: MySQL entity ID
     * @param $parent: jQuery selector to identify parent element to be refreshed after editing
     * @param $refresh_url: in case of asynchronously loaded content, URL where the new HTML can be fetched
     * @param $compact: false for button labels, true for only a pencil icon
     * @return string
     */
    /*public function get_entity($table, $id, $parent, $refresh_url, $compact) {
        if($this->edit_mode) {
            $CI = &get_instance();

            $table_name = ($table !== 'translations') ? $this->config['tables'][$table][1] : 'translations';

            $CI->db
                ->select('id')
                ->where('id', $id)
                ->from($table_name);

            $entity = $CI->db->get()->row();

            if (is_null($compact)) {
                $compact = !$this->config['button_labels'];
            }

            if(!empty($entity))
            {
                return $this->get_unleash_edit_link($this->config['tables'][$table][0], $entity->id, $parent, $refresh_url, $compact);
            }
        }
        return '';
    }*/

    /**
     * Generates Unleash popup URL
     * @param $table_id: Unleash table ID
     * @param $record_id: MySQL entry ID
     * @return string
     */
    public function get_unleash_popup_url($table_id = null, $record_id = null) {
        $segments = array(
            'http://www.unleash.be/popup?unleash_id=' . $this->config['website_id'],
        );

        if (!is_null($table_id)) {
            array_push($segments, '&table_id=' . $table_id);
            if (!is_null($record_id)) {
                array_push($segments, '&id=' . $record_id);
            }
        }
        return implode('', $segments);
    }

    /**
     * Generates direct unleash website URL
     * @return string
     */
    public function adminUrl() {
        return route(config($this->configName . '.admin_login_route'));
    }

    /**
     * Generates edit button
     * @param $table_id: Unleash table ID
     * @param $record_id: MySQL entry ID
     * @param $parent: jQuery selector to identify parent element to be refreshed after editing
     * @param $refresh_url: in case of asynchronously loaded content, URL where the new HTML can be fetched
     * @param bool $compact: false for button labels, true for only a pencil icon
     * @return string
     */
    
    public function editLink($recordId, $term, $parent = null, $refreshUrl = null, $compact = false) {

        $translation = DB::table($this->table)
            ->where('id', $recordId)
            ->first()
        ;

        if ($translation->type == 'seo') {
            $routeName = config($this->configName . '.seo_edit_route');
            $paramName = config($this->configName . '.seo_edit_route_parameter');
        } else {
            $routeName = config($this->configName . '.translation_edit_route');
            $paramName = config($this->configName . '.translation_edit_route_parameter');
        }

        $editUrl = route($routeName, [$paramName => $term]);
        $dataParent = !is_null($parent) ? ' data-inline-parent="' . $parent . '"' : '';
        $dataRefresh = !is_null($refreshUrl) ? ' data-inline-refresh-url="' . $refreshUrl . '"' : '';
        $dataContent = ' data-inline-content="#inline-edit-' . $recordId . '"';
        $classCompact = $compact ? ' inline-edit-link-compact' : '';
        return '<span id="inline-edit-link-' . $recordId .'"' . $dataParent . $dataRefresh . $dataContent .
            ' data-href="' . $editUrl . '" class="custom-btn custom-edit inline-edit-link' . $classCompact . 
            '"><i class="inline-edit-icon-pencil"></i>edit</span>';
    }

    /**
     * Generates CSS to apply accent color defined in libraries/unleash/config.php to elements
     * @return string
     */
    public function colorize()
    {
        // 	$accent_styles = array(
        //		'<css-property>' => array(
        //			'<adjust-brightness>' => array(
        //				'<css-selector>',
        //				'<css-selector>'
        //			)
        //		)
        //	)

        $accentStyles = array(
            'color' => array(
                '0' => array(
                    '.inline-edit-toolbar ul li a',
                    '.inline-edit-email-header .inline-edit-ul .inline-edit-li.inline-edit-to i[class^="inline-edit-icon"]:hover',
                    '.inline-edit-fancybox .fancybox-close:after'
                )
            ),
            'background-color' => array(
                '0' => array(
                    '.custom-btn',
                    '.btn-primary',
                    '.inline-edit-toolbar ul li.inline-edit-close-toggle a:hover',
                    '.inline-edit-email-wrapper .inline-edit-email-header .inline-edit-ul .inline-edit-li.inline-edit-email-addresses .inline-edit-span.inline-edit-highlight'
                ),
                '-35' => array(
                    '.custom-btn:hover',
                    '.custom-btn:focus'
                )
            ),
            'border-color' => array(
                '0' => array(
                    '.inline-edit-toolbar ul li.active a'
                )
            )
        );

        $html = '<style id="inline-edit-colorize">';
        foreach ($accentStyles as $property => $variants) {
            foreach ($variants as $deltaBrightness => $classes) {
                $html .= count($classes) > 0 ? Util::cssMultiSelector($classes) . '{' . $property . ':' .
                    Util::adjustBrightness(config($this->configName . '.color'), $deltaBrightness) . '}' : ''
                ;
            }
        }
        $html .= '</style>';

        return $html;
    }

    /**
     * Generates style links
     * @return string
     */
    public function styles()
    {
        if($this->editMode)
        {
            $output = '<link type="text/css" rel="stylesheet" media="screen" href="' . $this->assetPath('js/fancybox/jquery.fancybox.css') . '"/>';
            $output .= '<link type="text/css" rel="stylesheet" media="screen" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,600,400italic,600italic,700,300"/>';
            $output .= '<link type="text/css" rel="stylesheet" media="screen" href="' . $this->assetPath('css/fonts.css') . '"/>';
            $output .= '<link type="text/css" rel="stylesheet" media="screen" href="' . $this->assetPath('css/style.css') . '"/>';
            $output .= $this->colorize();
            return $output;
        }

        return '';
    }

    /**
     * Generates script links
     * @return string
     */
    public function scripts()
    {
        if($this->editMode)
        {
            $output = '<script src="' . $this->assetPath('js/fancybox/jquery.fancybox.pack.js') . '"></script>';
            $output .= '<script src="' . $this->assetPath('js/tinycolor.min.js') . '"></script>';
            $output .= '<script src="' . $this->assetPath('js/chart.js') . '"></script>';
            $output .= '<script src="https://apis.google.com/js/client.js"></script>';
            $output .= '<script src="' . $this->assetPath('js/webshop-inline-editing.js') . '"></script>';
            return $output;
        }

        return '';
    }

    /**
     * Concatenates style & script links
     * @return string
     */
    public function assets()
    {
        return $this->styles() . $this->scripts();
    }

    /**
     * Displays toolbar according to configuration in libraries/unleash/config.php
     * @param null $menu
     * @param bool $analytics: Set to false
     * @return string
     */
    public function toolbar($menu = null, $analytics = false)
    {
        if($this->editMode) {

            $enabledFeatures = 0;
            foreach (config($this->configName . '.features') as $feature => $enabled) {
                if ($enabled) $enabledFeatures++;
            }

            $html = '<style>.inline-edit-toolbar:hover{-webkit-transform: translate3d(-' . (($enabledFeatures+2)*40) . 'px, 0, 0);-moz-transform: translate3d(-' . (($enabledFeatures+2)*40) . 'px, 0, 0);-ms-transform: translate3d(-' . (($enabledFeatures+2)*40) . 'px, 0, 0);-o-transform: translate3d(-' . (($enabledFeatures+2)*40) . 'px, 0, 0);transform: translate3d(-' . (($enabledFeatures+2)*40) . 'px, 0, 0);}</style>'.
                '<div class="inline-edit-widget inline-edit-toolbar' . ($analytics ? ' inline-edit-has-analytics' : '') . '" style="width: ' . (($enabledFeatures+2)*40) . 'px">' .
                '<a href="' . $this->adminUrl() . '" class="inline-edit-logo" title="Admin" ><i class="inline-edit-icon-esign"></i></a>' .
                '<ul style="width: ' . (($enabledFeatures+1)*40) . 'px">';

            if (config($this->configName . '.features.inline-editing')) {
                $html .= '<li class="inline-edit-toggle active" title="Edit"><a><i class="inline-edit-icon-pencil"></i></a></li>';
            }

            if (config($this->configName . '.features.seo')) {
                $html .= '<li class="inline-edit-seo-toggle" title="SEO"><a><i class="inline-edit-icon-google"></i></a></li>';
            }

            $html .=
                '<li class="inline-edit-close-toggle" title="Stop editing"><a href="' . route('webshop-inline-editing.disable') . '"><i class="inline-edit-icon-times"></i></a></li>' .
                '</ul>' .
                '</div>';

            if (config($this->configName . '.features.seo')) {

                $titleTranslation = DB::table($this->table)
                    ->whereNull('table')
                    ->whereNull('row')
                    ->where('locale', $this->locale)
                    ->where('column', $menu . '.seo_title')
                    ->first()
                ;

                if (is_null($titleTranslation)) {
                    $titleTranslation = DB::table($this->table)
                        ->insert([
                            'locale' => $this->locale,
                            'column' => $menu . '.seo_title',
                            'type' => 'seo'
                        ]);
                    $titleTranslation = DB::table($this->table)
                        ->whereNull('table')
                        ->whereNull('row')
                        ->where('locale', $this->locale)
                        ->where('column', $menu . '.seo_title')
                        ->first()
                    ;
                }

                $seoTitleVal = $titleTranslation->value;
                $siteName = config('site.title');

                if (empty($seoTitleVal)) {
                    $seoTitleDisplay = htmlentities('###NOT SET###');
                } else {
                    if ($siteName) {
                        $seoTitleDisplay = $seoTitleVal . ' | ' . $siteName;
                    } else {
                        $seoTitleDisplay = $seoTitleVal;
                    }
                }

                $descriptionTranslation = DB::table($this->table)
                    ->whereNull('table')
                    ->whereNull('row')
                    ->where('locale', $this->locale)
                    ->where('column', $menu . '.seo_description')
                    ->first()
                ;

                if (is_null($descriptionTranslation)) {
                    $descriptionTranslation = DB::table($this->table)
                        ->insert([
                            'locale' => $this->locale,
                            'column' => $menu . '.seo_description',
                            'type' => 'seo'
                        ]);
                    $descriptionTranslation = DB::table($this->table)
                        ->whereNull('table')
                        ->whereNull('row')
                        ->where('locale', $this->locale)
                        ->where('column', $menu . '.seo_description')
                        ->first()
                    ;
                }
                
                $seoDescriptionVal = $descriptionTranslation->value;

                $seoDescriptionDisplay = strlen($seoDescriptionVal) > 156 ? substr($seoDescriptionVal, 0, 156) . ' ...' : $seoDescriptionVal;
                $seoDescriptionDisplay = strlen($seoDescriptionDisplay) > 0 ? $seoDescriptionDisplay : htmlentities('###NOT SET###');

                $html .= '<div class="inline-edit-widget inline-edit-seo">' .
                    '<div class="inline-edit-google-title">' . $this->editLink($titleTranslation->id, $titleTranslation->column, null, null, true) . '<span id="inline-edit-' . $titleTranslation->id . '">' . $seoTitleDisplay . '</span></div>' .
                    '<div class="inline-edit-google-url">' . $_SERVER['HTTP_HOST'] . preg_replace('/\/?\?edit=.*/', '', $_SERVER['REQUEST_URI']) . '</div>' .
                    '<div class="inline-edit-google-description">' . $this->editLink($descriptionTranslation->id, $descriptionTranslation->column, null, null, true) . '<span id="inline-edit-' . $descriptionTranslation->id . '">' . $seoDescriptionDisplay . '</span></div>' .
                    '</div>'
                ;
            }

            return $html;
        }

        return '';
    }

}