<?php
/**
 * Created by PhpStorm.
 * User: bartdecorte
 * Date: 03/12/2016
 * Time: 11:32
 */

namespace Esign\WebshopInlineEditing;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Request;

class WebshopInlineEditingController extends Controller {

    public function enable() {
        session(['webshop-inline-editing' => true]);
        // TODO fix
		if (session('_previous')['url'] != Request::url() && !preg_match('/^.*\/admin.*$/', session('_previous')['url'])) {
            return back();
        }
        return redirect('/');
    }

    public function disable() {
        session(['webshop-inline-editing' => false]);
        if (session('_previous')['url'] != Request::url() && !preg_match('/^.*\/admin.*$/', session('_previous')['url'])) {
            return back();
        }
        return redirect()->route(config('webshop-inline-editing.admin_login_route'));
    }

}