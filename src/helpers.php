<?php
/**
 * Created by PhpStorm.
 * User: bartdecorte
 * Date: 04/12/2016
 * Time: 16:22
 */

if (!function_exists('inlineEditing')) {

    function inlineEditing() {
        return app('webshopinlineediting');
    }

}

if (!function_exists('ie')) {

    function ie ($term, $parent = null, $refresh_url = null, $variables = []) {
        return inlineEditing()->render($term, $parent, $refresh_url, $variables);
    }

}