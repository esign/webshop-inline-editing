<?php
/**
 * Created by PhpStorm.
 * User: bartdecorte
 * Date: 03/12/2016
 * Time: 09:53
 */

namespace Esign\WebshopInlineEditing;

use Illuminate\Support\ServiceProvider;

class WebshopInlineEditingServiceProvider extends ServiceProvider {

    public function boot()
    {
        $this->publishes([
            __DIR__ . '/../config/config.php' => config_path('webshop-inline-editing.php'),
        ], 'config');

        $this->publishes([
            __DIR__ . '/../assets' => public_path('esign/webshop-inline-editing'),
        ], 'public');

        if (! $this->app->routesAreCached()) {
            require __DIR__ . '/routes.php';
        }
    }

    public function register()
    {
        $packageConfigFile = __DIR__ . '/../config/config.php';

        $this->mergeConfigFrom(
            $packageConfigFile, 'webshop-inline-editing'
        );

        $this->app->singleton(WebshopInlineEditing::class, function ($app) {
                return new WebshopInlineEditing();
            }
        );

        $this->app->alias(WebshopInlineEditing::class, 'webshopinlineediting');
    }

}