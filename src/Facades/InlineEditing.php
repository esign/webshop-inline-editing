<?php
/**
 * Created by PhpStorm.
 * User: bartdecorte
 * Date: 03/12/2016
 * Time: 10:04
 */

namespace Esign\WebshopInlineEditing\Facades;

use Esign\WebshopInlineEditing\WebshopInlineEditing;
use Illuminate\Support\Facades\Facade;

class InlineEditing extends Facade {

    protected static function getFacadeAccessor()
    {
        return WebshopInlineEditing::class;
    }

}